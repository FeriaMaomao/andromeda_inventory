## Grupo 2.

## ¿Integrantes del grupo 2?

- Juan Felipe Oquendo López - 1721021631
- Mauricio Alejandro González Feria - 100239358
- Pablo Albeiro Valencia Aguirre - 1921982553
- William Camilo Aucique Silva - 2011981760

## ¿Qué proyecto de software realizarán para el Módulo y bajo qué lenguaje de programación?

* El proyecto que se realizará a cargo de este grupo será un sistema encargado de registrar y almacenar la información de los activos tecnológicos en las empresas. Este proyecto se realizará bajo la siguiente arquitectura: 
- En el frontend se usará HTML5, CSS3, JavaScript, JQuery.
- El lenguaje de programación utilizado en el proyecto será PHP.
- La base de datos a utilizar será MySQL. 
- El control de versiones se hará Git.
- El repositorio de código será Bitbucket.
- El CI/CD elegido será Jenkins.
- El proyecto sera alojado en un servidor en la nube y su despliegue se realizará con contenedores Docker.
- Se usará linux server para los contenedores,

## ¿Por qué seleccionaron ese tipo de proyecto de software?

* Las pymes constantemente se deben enfrentar a la ilegalidad que implica descargar ilícitamente programas de internet para poder poner en marcha su negocio. 
* Las autoridades estiman que el 54 % del software que se comercializa es ilegal y buena parte de este uso indebido de programas se relaciona con empresas pequeñas que no cuentan con los recursos suficientes para costear una licencia legal.
* Los dispositivos móviles y los computadores son herramientas valiosas para que una PYME pueda fomentar el esfuerzo colectivo y aumentar su rendimiento, por tal motivo buscamos incentivar a las empresas para que adopten la tecnología y tengan a la mano un software completo, legal, libre y de código abierto que les ayude a tecnificar sus nventarios tecnológico; Al mismo tiempo realizar nuestra contribuición al software libre.

